#include <mag3110.h>
#include <SPI.h>
#include <Ethernet.h>
#include<Wire.h>
#include <adafruitDHT.h>
#include <mag3110.h>
#include <Adafruit_BMP085.h>
#define DHTPIN 9
#define DHTTYPE 22
#define MILLISECONDS_BETWEEN_READINGS 2000
#define SETUP_DELAY 2000
#define Serial_Rate 9600

#define UDP_PORT 30001

char outputBuffer[127];
char outCount;
//char *Password = "Prototype";
//int PasswordLength = 10;
const String sensorName = "Prototype";
long lastReadingTime = 0;

byte MacAddress[] = {0x90, 0xA2, 0xDA, 0x0E, 0x50, 0xC3};
byte ip[] = {192, 168, 100, 123};
byte gateway[] = {129, 168, 100, 8};
byte server[] = {10, 8, 105, 2};
byte dnsIp[] = {10, 10, 7, 6};

//EthernetUDP udp;
EthernetServer eServer = EthernetServer(80);

DHT dht(DHTPIN, DHTTYPE);
Adafruit_BMP085 bmp;

float DHTTemp = 0;
float DHTHumidity = 0;
int32_t pressure;

/*---------------------------------------------------------------------------*/
void setup()
{
  pinMode(10, OUTPUT);
  Serial.begin(Serial_Rate);
  Wire.begin();
  bmp.begin();
  dht.begin();
  Ethernet.begin(MacAddress, ip, gateway, dnsIp);
  eServer.begin();
  delay(SETUP_DELAY);
//  udp.begin(UDP_PORT);
}
void loop()
{  
  if(millis() - lastReadingTime > MILLISECONDS_BETWEEN_READINGS)
  {
    lastReadingTime = millis();
    DHTTemp     = CelsToFahren(dht.readTemperature());
    DHTHumidity = dht.readHumidity();
    pressure    = bmp.readPressure();
    sprintf(outputBuffer, "Temperature: %i Fahrenheit\nHumidity: %i %%RH\nPressure: %ld Pascals", int(DHTTemp), int(DHTHumidity), pressure);
    Serial.println(outputBuffer);
    
  }
  listenForClients();
//  int packetSize = udp.parsePacket();
//  if(packetSize)
//  {
//    IPAddress clientIP   = udp.remoteIP();
//    int clientPort       = udp.remotePort();
//  }
}
void listenForClients()
{
  EthernetClient client = eServer.available();
  if(client)
  {
    Serial.println("got a client");
    boolean currentLineIsBlank = true;
    Serial.print("current line");
    while(client.connected())
    {
      if(client.available())
      {
        char c = client.read();
        if(c == '\n' && currentLineIsBlank)
        {
//          sprintf(outputBuffer, "Temperature %d\nHumidity %d\nPressure %d", DHTTemp, DHTHumidity, pressure);
          client.println("HTTP/1.1. 200 OK");
          client.println("Content-Type: text/plain");
          client.println("Connection: close");
          client.println();
          client.print("Sensor Name: ");
          client.println(sensorName);
          client.write(outputBuffer);
          Serial.println(outputBuffer);
          break;
        }
        if (c == '\n')
          currentLineIsBlank = true;
        else if (c != '\r')
          currentLineIsBlank = false;
      }
    }
    delay(1);
    client.stop();
  }
}



float CelsToFahren(float celsTemp){
  return (celsTemp*9/5) + 32;
}

//grabs the most recent readings from the sensors, and packs them into a buffer for UDP transfer. 
//char *MakePacketBuffer(int passwordLength,char* password)
//{
//  char packetBuffer[127]; //Can currently fit into 127 bytes, but make sure to keep this below 576 to assure networks accept it.
//  
//  sprintf(packetBuffer,
//      "Sensor-Name: %d temperature: %d humidity: %d presssure: %d",
//      SensorName,
//      (int)CelsToFahren(dht.readTemperature()), 
//      (int)dht.readHumidity(), 
//      (int)bmp.readPressure()
//  );
//  return packetBuffer;
//}
//boolean verifyPassword(int pwdLength, char *pwd)
//{
//  if(pwdLength != PasswordLength)
//    return false;
//  for(int x = 0; x < pwdLength;x++)
//  {
//    if(pwd[x] != Password[x])
//      return false;
//  }
//  return true;
//}
