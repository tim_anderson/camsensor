#include <SPI.h>
#include <Ethernet.h>
#include<Wire.h>
#include<SD.h>
#include <adafruitDHTOptomized.h>
#include <mag3110.h>
#include <Adafruit_BMP085Optomized.h>
#define DHTPIN 9
#define DHTTYPE 22
#define MILLISECONDS_BETWEEN_READINGS 2000
#define SETUP_DELAY 2000
#define Serial_Rate 9600

char netOutBuffer[128];
char outCount;

const String SensorName = "Proto";

byte MacAddress[] = {0x90, 0xA2, 0xDA, 0x0E, 0x50, 0xC3};
byte ip[] = {192, 168, 100, 123};
byte gateway[] = {129, 168, 100, 8};
byte server[] = {10, 8, 105, 2};
byte dnsIp[] = {10, 10, 7, 6};
char *fileName = "l.txt";

EthernetClient client;
EthernetClient dclient;

const String ftpLocString = "sensor-data/";

//char *fileWriteErrorIndicatorFile = "i.dat";
File datafile;

DHT dht(DHTPIN, DHTTYPE);
Adafruit_BMP085 bmp;


//int xValue;
//int yValue;
//int zValue;
int DHTTemp = 0;
int DHTHumidity = 0;
//int temp;
int32_t pressure;
byte readingsPerSend = 10;
byte savedReadings = 0;

/*---------------------------------------------------------------------------*/
void setup()
{
  pinMode(10, OUTPUT);
  Serial.begin(Serial_Rate);
  Wire.begin();
  SD.begin(4);
  bmp.begin();
//  CalibrateMAG();
  dht.begin();
  delay(SETUP_DELAY);
  Ethernet.begin(MacAddress, ip, gateway, dnsIp);
//  Ethernet.begin(MacAddress);
}
void loop()
{  
    delay(MILLISECONDS_BETWEEN_READINGS);
  DHTTemp        =         dht.readTemperature();
  DHTHumidity    =         dht.readHumidity();
  pressure       =         bmp.readPressure();
  
//  DHTTemp      =         CtoF(dht.readTemperature());
//  temp         =         bmp.readTemperature();
//  xValue       =         GetXValueMAG();
//  yValue       =         GetYValueMAG();
//  zValue       =         GetZValueMAG();

  datafile = SD.open(fileName, FILE_WRITE);
  if(datafile)
  {
    datafile.write("\n");
    datafile.write(" tmp: ");
    datafile.print(DHTTemp);
    datafile.write(" hu: ");
    datafile.print(DHTHumidity);
    datafile.print(" pres: ");
    datafile.print(pressure);
//    datafile.write(" x: ");
//    datafile.print(xValue);
//    datafile.write(" y: ");
//    datafile.print(yValue);
//    datafile.write(" z: ");
//    datafile.print(zValue);
//    Serial.println("OK");
  }
  else
  {
    Serial.println("could not write to file D:");
  }
  //increment saved readings, and loop it back to 0 when it hits readingsPerSend
  savedReadings = (savedReadings + 1) % readingsPerSend;
  if(!savedReadings)
  {
    savedReadings = 0;
    Serial.println("~");
    Serial.println(sendResultsOverNetwork(datafile));
    SD.remove(fileName);
  }
  datafile.close();

}

//error codes, -1 = datafile no opened, -2 = bad connect, -3 = ervc
// -4 - -7 = ftp authentication err, -8 = port err, 
//-30 is problem on rewinding the file.
//-32 is a problem in the tStr for loop.
int sendResultsOverNetwork(File datafile)
{
  byte test = 1;
  if(!datafile.seek(0))
    return -30;
//  Serial.println(test++);
  if (!datafile)  //the datafile must have been opened sucessfully
    return -1;

  Serial.println(test++);
  if(!client.connect("http://www.google.com", 80))
    return -2;
  Serial.println("sucessfully connected");
  client.stop();
  return 1;
  
  if(!client.available())
    return -20;
  
  Serial.println(test++);
  if(!eRcv()) return -3;
  
  Serial.println(test++);
  client.println(F("USER cams"));
  if(!eRcv()) return -4;
  
  Serial.println(test++);
  client.println(F("PASS ntsg123"));
  if(!eRcv()) return -5;

  
  client.println(F("SYST"));
  if(!eRcv()) return -6;

  Serial.println(test++);
  client.println(F("PASV"));
  if(!eRcv()) return -7;
  
  Serial.println(test++);
  char *tStr = strtok(netOutBuffer, "(,");
  int arrayPasv[6];
  for (int i = 0; i <6;i++)
  {
    Serial.println(i);
    tStr = strtok (NULL, "(," );
    arrayPasv[i] = atoi(tStr);
    if(tStr == NULL)
    return -32;
  }
  Serial.println(test++);
  unsigned int hiPort, loPort;      //I have no idea why these are here. Q_Q
  hiPort = arrayPasv[4]<<8;        //what is this
  loPort = arrayPasv[5] & 255;      //i dont even
  hiPort = hiPort | loPort;          //still dont even
  if(!dclient.connect(server, hiPort))
  {
    client.stop();
    return -8;
  }
  client.print(F("STOR "));
  char* millisStr;
  ltoa(millis(), millisStr, 10);    //use millis() as a file guid
  client.println(ftpLocString + SensorName + millisStr +".txt");
  if(!eRcv())
  {
    dclient.stop();
    return -3;
  }
  byte clientBuf[64];
  int clientCount = 0;
  while (datafile.available())
  {
    clientBuf[clientCount] = datafile.read();
    clientCount++;
    if(clientCount>63)
    {
      dclient.write(clientBuf, 64);
      clientCount=0;
    }
  }
  if(clientCount >0) 
    dclient.write(clientBuf, clientCount);    //write any part left from the file
  dclient.stop();
  if(!eRcv()) return -1;
  client.println(F("QUIT"));
  if(!eRcv()) return -1;
  client.stop();
  return 1001;  
}

byte eRcv()
{
  byte respCode;
  byte thisByte;
  while(!client.available())
    delay(1);
  respCode = client.peek();
  outCount = 0;
  while(client.available())
  {
    thisByte = client.read();
    if(outCount <127)
    {
      netOutBuffer[outCount] = thisByte;
      outCount++;
      netOutBuffer[outCount] = 0;       //reinit the next byte
    }
  }
  if(respCode >='4')
    return 0;
  else
    return 1;
}

//float CtoF(float celsTemp){
//  return (celsTemp*9/5) + 32;
//}

//void printResultsOnSerial(){
//  if(isnan(DHTTemp) || isnan(DHTHumidity))
//    Serial.println("DHT err.");
//  Serial.print(" temp: ");
//  Serial.print(DHTTemp);
//  Serial.print(" humidity: ");
//  Serial.print(DHTHumidity);
//  Serial.print(" Pressure: ");
//  Serial.print(pressure);
//  Serial.print(" x: ");
//  Serial.print(xValue);
//  Serial.print(" y: ");
//  Serial.print(yValue);
//  Serial.print(" z: ");
//  Serial.print(zValue);
//  Serial.println("");
//}


